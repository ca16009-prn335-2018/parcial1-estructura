#include <stdio.h>

void main(){
	int n;
	printf("Ingrese el tamaño de la matriz\n");
	scanf("%d", &n);
	int matriz[n][n];
	
	printf("Ingrese los datos de la matriz\n");
	for(int f=0;f<n;f++){
		for(int c=0;c<n;c++){
			printf("Ingrese el valor de (%d,%d)\n",f,c);
			scanf("%d", &matriz[f][c]);
		}
	}
	
	printf("-----MATRIZ-----\n");
	for(int f=0;f<n;f++){
		for(int c=0;c<n;c++){
			printf("%d\t", matriz[f][c]);
		}
		printf("\n");
	}
	
	int menor=matriz[0][0],mayor=matriz[0][0], colmenor,filmayor;
	
	
	
	for(int f=0;f<n;f++){
		for(int c=0;c<n;c++){
			if(matriz[f][c]<menor){
				menor=matriz[f][c];
				colmenor=c;					
			}
		}
	}
	for(int f=0;f<n;f++){
		if(menor>matriz[f][colmenor]){
			mayor=matriz[f][colmenor];
			filmayor=f;
		}
	}

	
	if(mayor==menor){
		printf("El punto de silla es %d, en la posicion (%d,%d)",matriz[filmayor][colmenor],filmayor,colmenor);		
	}else{
		printf("No existe punto de silla");
	}


}
